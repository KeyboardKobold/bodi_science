package bodi_science_kweb

import kweb.Kweb
import kweb.h1
import kweb.h2
import kweb.new

fun main() {
    Kweb(port = 80) {
        doc.body.new {
            h1().text("Hello World!")
            h2().text("Welcome to bodi.science!")
        }
    }
}